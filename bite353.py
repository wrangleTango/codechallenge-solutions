import typer  # use typer.run and typer.Argument


def sum_numbers(a: int, b: int):
    return a + b


def main(a: int = typer.Argument("The value of the first summand"),
         b: int = typer.Argument("The value of the second summand")):
    print(sum_numbers(a, b))  # edit this


if __name__ == "__main__":
    typer.run(main)
