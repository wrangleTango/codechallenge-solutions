from typing import Dict, Set


def filter_bites(
    bites: Dict[int, str] = DEFAULT_BITES,
    bites_done: Set[int] = EXCLUDE_BITES
) -> Dict[int, str]:
    bitten_out = {bite:bites[bite] for bite in bites if bite not in bites_done}
    print(bitten_out)
    return bitten_out


