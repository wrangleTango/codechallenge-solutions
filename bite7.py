from datetime import datetime
import os
import urllib.request
import pprint


def convert_to_datetime(line):
    return datetime.strptime(line.split()[1], "%Y-%m-%dT%H:%M:%S")

def time_between_shutdowns(loglines):
    times_list = []
    #for line in loglines:
    #    if SHUTDOWN_EVENT in line:
    #        times_list.append(line.split()[1])
    times_list = [line for line in loglines if SHUTDOWN_EVENT in line]
    time_chunks = []
    for time in times_list:
        time_chunks.append(convert_to_datetime(time))
    return time_chunks[1] - time_chunks[0]

