def positive_divide(numerator, denominator):
    try:
        output = numerator / denominator
    except ZeroDivisionError: return 0
    except TypeError: return TypeError
    if output :
        # need to raise a value error but don't know how
        # I was able to print a ValueError type object
        # but this is rejected by the pytest.
        raise ValueError
    else:
        return output

