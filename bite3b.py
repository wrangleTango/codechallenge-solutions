import os
import urllib.request


def load_words():
    fout = []
    with open(DICTIONARY, 'r') as f:
        for word in f:
            fout.append(word.replace('\n',""))
    return fout


def calc_word_value(word):
    word_split = list(word)
    word_score = 0
    for letter in word_split:
        for score_group, letter_group in enumerate(scrabble_scores, start=0):
            if letter.upper() in letter_group[1].split():
                word_score += scrabble_scores[score_group][0]
    return word_score


def max_word_value(words):
    collected_words = []
    for entry in words:
        combined_entry = [calc_word_value(entry), entry]
        collected_words.append(combined_entry)
    collected_words.sort(key=lambda score: score[0], reverse=True)
    return(collected_words[0][1])
    
