def is_armstrong(n: int) -> bool:
    return sum([int(d) ** len(str(n)) for d in str(n)]) == n

print(is_armstrong(153))
