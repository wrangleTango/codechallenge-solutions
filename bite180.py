from collections import defaultdict


def group_names_by_country(data: str = data) -> defaultdict:
    countries = defaultdict(list)
    for person in data.splitlines()[1:]:
        last_name, first_name, country = person.split(',')
        countries[country] += [first_name + " " + last_name]
    return countries
        
