from functools import wraps


def login_required(func):
    @wraps(func)
    def wrapper(*args, **kwargs):
        if args[0] in loggedin_users:
            return func(*args, **kwargs)
        elif (args[0] in known_users) and (args[0] not in loggedin_users):
            return "please login"
        else:
            return "please create an account"
    return wrapper

        


@login_required
def welcome(user):
    '''Return a welcome message if logged in'''
    return f"welcome back {user}"

