import random


class NoBitesAvailable(Exception):
    """There are no more Bites available to pick from"""


class Promo:

    def __init__(self):
        self.all_bites = BITES.copy()
        self.bites_done = BITES_DONE.copy()

    def _pick_random_bite(self):
        if len(set(self.bites_done)) < len(BITES.keys()):
            return int(random.choice(list(BITES.keys() - self.bites_done)))
        elif len(self.bites_done) == len(BITES.keys()):
            raise NoBitesAvailable
            
    
    def new_bite(self):
        new_pick = self._pick_random_bite()
        self.bites_done.add(new_pick)
        return new_pick

