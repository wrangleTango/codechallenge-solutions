from itertools import combinations as combine

def find_number_pairs(numbers, N=10):
    return [combo for combo in combine(numbers, 2) if sum(combo) == N]
    
