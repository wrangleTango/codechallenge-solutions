from re import findall as fa


def generate_affiliation_link(url):
    return "http://www.amazon.com" + fa(r'/dp/\d*\w', url)[0] + "/?tag=pyb0f-20"


