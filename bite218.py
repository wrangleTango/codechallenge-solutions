from functools import wraps


def sandwich(func):
    @wraps(func)
    def wrapped(*args, **kwargs):
        print(UPPER_SLICE)
        result = func(*args, **kwargs)
        print(LOWER_SLICE)
        return result
    return wrapped
