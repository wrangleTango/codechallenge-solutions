import os
import statistics
from urllib.request import urlretrieve
from collections import Counter


def get_all_line_counts(data: str = STATS) -> list:
    # TODO 1: get the 186 ints from downloaded STATS file
    with open(STATS) as fout:
        return [int(line.split()[0]) for line in fout.readlines()]



def create_stats_report(data):
    if data is None:
        # converting to a list in case a generator was returned
        data = list(get_all_line_counts())

    # taking a sample for the last section
    sample = list(data)[::2]

    # TODO 2: complete this dict, use data list and
    # for the last 3 sample_ variables, use sample list
    data_counter = Counter(data)
    sample_counter = Counter(sample)
    stats = dict(count=data_counter.total(),
                 min_=min(data_counter.most_common())[0],
                 max_=max(data_counter.most_common())[0],
                 mean=statistics.mean(data),
                 pstdev=statistics.pstdev(data),
                 pvariance=statistics.pvariance(data),
                 sample_count=sample_counter.total(),
                 sample_stdev=statistics.stdev(sample),
                 sample_variance=statistics.variance(sample),
                 )
    return STATS_OUTPUT.format(**stats)

