from operator import itemgetter

def dedup_and_title_case_names(names):
    """Should return a list of title cased names,
       each name appears only once"""
    name_out = []
    for person in names:
        if person.title() not in name_out:
            name_out.append(person.title())
    return name_out
 
print(dedup_and_title_case_names(NAMES))

def sort_by_surname_desc(names):
    """Returns names list sorted desc by surname"""
    names = dedup_and_title_case_names(names)
    for i, person in enumerate(names, start=0):
        names[i] = person.split(" ")
    get_surname = itemgetter(1)
    names = sorted(names, key=get_surname)
    return names

print(sort_by_surname_desc(NAMES))
namely = sort_by_surname_desc(NAMES)
print(namely[][0:])

def shortest_first_name(names):
    """Returns the shortest first name (str).
       You can assume there is only one shortest name.
    """
    names = dedup_and_title_case_names(names)
    return min(names[:0])

print(shortest_first_name(NAMES))
    # ...
