from collections import Counter

def freq_digit(num: int) -> int:
    the_count = Counter(str(num)).most_common(1)
    return int(the_count[0][0])

