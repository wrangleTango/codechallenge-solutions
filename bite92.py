from collections import namedtuple
from datetime import datetime, timedelta



def pretty_date(date):
    difference = NOW - date
    if type(date) != datetime or date > NOW:
        raise ValueError
    for time_bound in TIME_OFFSETS:
        if timedelta(seconds=time_bound.offset) > difference and (difference < timedelta(MINUTE *2)):
            return f"{time_bound[1]}".replace('{}', str(difference.seconds // MINUTE))
        elif timedelta(seconds=time_bound.offset) > difference and (difference < HOUR*2):
            return f"{time_bound[1]}".replace('{}', str(difference.seconds // HOUR))
        elif timedelta(seconds=time_bound.offset) > difference and (difference < DAY):
            return f"{time_bound[1]}".replace('{}', difference.seconds // DAY)
        
result = pretty_date(NOW - timedelta(hours=16))
print(result)
