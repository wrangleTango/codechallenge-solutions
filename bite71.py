class RecordScore():
    def __init__(self):
        self.record = None
        
    def __call__(self, score):
        if self.record == None:
            self.record = score
        else:
            self.record = max([score, self.record])
        return self.record
    
