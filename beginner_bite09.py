def get_workout_motd(day):
    day_passed = day.title()
    if day_passed.isalpha() == True and day_passed in WORKOUT_SCHEDULE.keys():
        if WORKOUT_SCHEDULE[day_passed] != REST:
            return TRAIN.format(WORKOUT_SCHEDULE[day_passed])
        else:
            return CHILL_OUT
    else:
        return INVALID_DAY

