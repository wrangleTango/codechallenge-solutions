from datetime import datetime, timedelta


def gen_special_pybites_dates():
    hundred_days = timedelta(days=100)
    date_jump = PYBITES_BORN
    while True:
        date_jump += hundred_days
        yield date_jump
        
