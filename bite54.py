import textwrap
from pprint import pprint


def print_hanging_indents(poem):
    step_one = textwrap.dedent(poem.replace("\n\n", "\n"))
    print(textwrap.indent(step_one, prefix="    ", predicate=lambda line:line.startswith(("G", "W", "Y", "O"))))


