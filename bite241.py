import pytest
from typing import List


def list_to_decimal(nums: List[int]) -> int:
    for num in nums:
        if isinstance(num, bool) or not isinstance(num, int):
            raise TypeError
        elif not num in range(0, 10):
            raise ValueError

    return int(''.join(map(str, nums)))


def test_over_range():
    with pytest.raises(ValueError):
        list_to_decimal([11, 7, 8, 20])

def test_just_over_range():
    with pytest.raises(ValueError):
        list_to_decimal([2, 5, 6, 10])
        
def test_under_range():
    with pytest.raises(ValueError):
        list_to_decimal([-1, 2, 4, -9])

def test_way_under_range():
    with pytest.raises(ValueError):
        list_to_decimal([2, 5, 7, -200])
        
def test_under_range_two():
    with pytest.raises(ValueError):
        list_to_decimal([3, 3, -4, 7])

def test_string_in():
    with pytest.raises(TypeError):
        list_to_decimal([6, "3", 3])

def test_string_in_alpha():
    with pytest.raises(TypeError):
        list_to_decimal([2, 5, 6, 'cat'])

def test_float_in():
    with pytest.raises(TypeError):
        list_to_decimal([4.2, 2, 2, 5.0])
        
def test_bool_in():
    with pytest.raises(TypeError):
        list_to_decimal([True, 3, 4, 1])

def test_output():
    assert list_to_decimal([5]) == 5

def test_output_multi():
    assert list_to_decimal([4, 7, 9]) == 479

def test_long_output():
    assert list_to_decimal([5, 7, 3, 5, 9, 0, 1, 2, 4, 4]) == 5735901244
    
