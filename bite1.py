def sum_numbers(numbers=None):
    sum_sequence = 0
    if numbers == None:
        for i in range(1,101):
            sum_sequence += i
    else:
        for i in numbers:
            sum_sequence += i
    return sum_sequence

print(sum_numbers())
print(sum_numbers([1,3,5,7,9,11]))
