import re
from pprint import pprint as pp


def split_and_trim(quote_and_author):
        quote_and_author = quote_and_author.split(" - ")
        quote_and_author[0] = quote_and_author[0].split('"')[1]
        return quote_and_author
    
def extract_quotes(html: str = HTML) -> dict:
    split_list, quote_dict = HTML.split("</p>"), {}
    p_pattern = re.compile('<p>\\d')
    return {split_and_trim(line)[1]: split_and_trim(line)[0] for line in split_list if p_pattern.search(line)}


