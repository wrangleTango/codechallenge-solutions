import re


def validate_license(key: str) -> bool:
    key_compare = re.compile('PB(-[A-Z0-9]{8,8}){4,4}')
    if key_compare.fullmatch(key):
        return True
    else:
        return False

