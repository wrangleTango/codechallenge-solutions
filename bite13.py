from collections import namedtuple
from datetime import datetime
import json


def dict2nt(dict_):
    arg_builder = [val for key, val in dict_.items()]
    return blog_tuple._make(arg_builder)

def nt2json(nt):
    converted_nt = nt._asdict()
    for key in converted_nt:
        if type(converted_nt[key]) == datetime:
            converted_nt[key] = str(converted_nt[key])
    return json.dumps(converted_nt)
    
