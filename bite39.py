from datetime import datetime, timedelta
import os
import re
from typing import List
import urllib.request


def get_all_timestamps() -> List[str]:
    with open(COURSE_TIMES) as fout:
        data = fout.read()
    pattern = re.compile('\d+:\d+')
    return pattern.findall(data)


def calc_total_course_duration(timestamps) -> str:
    stamp_split = [stamp.split(":") for stamp in timestamps]
    min_sum, sec_sum = timedelta(), timedelta()
    for stamp in stamp_split:
        min_sum += timedelta(minutes=int(stamp[0]))
        sec_sum += timedelta(seconds=int(stamp[1]))
    return str((min_sum + sec_sum))

