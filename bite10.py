def divide_numbers(numerator, denominator):
    try:
        int(numerator), int(denominator)
    except TypeError:
        raise("TypeError from supplied inputs")
    try:
        divisor = int(numerator) / int(denominator)
    except ZeroDivisionError:
        return 0
    return divisor

        
