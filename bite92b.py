from collections import namedtuple
from datetime import datetime, timedelta


def pretty_date(date):
    try:
        if date > NOW or type(date) != datetime:
            raise ValueError
    except:
        raise ValueError
    for bound in TIME_OFFSETS:
        if NOW - date < timedelta(seconds=bound.offset) and bound.divider != None:
            return bound.date_str.replace("{}", str(int((NOW - date).seconds / bound.divider)))
        elif NOW - date < timedelta(seconds=bound.offset) and bound.divider == None:
            return bound.date_str.replace("{}", str((NOW - date).seconds))
        elif NOW - date >= timedelta(days=2):
            return date.strftime("%m/%d/%y")

