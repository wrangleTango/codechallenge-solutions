from functools import wraps


def make_html(element):
    def wrapper_html(func):
        @wraps(func)
        def wrapper_in_html(*args, **kwargs):
            tag_join = "<" + element + ">"
            return tag_join + func() + tag_join.replace("<", "</")
        return wrapper_in_html
    return wrapper_html  

@make_html('p')
@make_html('strong')
def get_text(text='I code with PyBites'):
    return text

