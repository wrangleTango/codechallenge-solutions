import os
import urllib.request


def load_words():
    fout = []
    with open(DICTIONARY, 'r') as f:
        for word in f:
            fout.append(word.replace('\n',""))
    return fout


def calc_word_value(word):
    word_split = list(word)
    word_score = 0
    for letter in word_split:
        if letter.upper() in scrabble_scores[0][1]:
            word_score += 1
        elif letter.upper() in scrabble_scores[1][1]:
            word_score += 2
        elif letter.upper() in scrabble_scores[2][1]:
            word_score += 3
        elif letter.upper() in scrabble_scores[3][1]:
            word_score += 4
        elif letter.upper() in scrabble_scores[4][1]:
            word_score += 5
        elif letter.upper() in scrabble_scores[5][1]:
            word_score += 8
        elif letter.upper() in scrabble_scores[6][1]:
            word_score += 10
    return word_score


def max_word_value(words):
    collected_words = []
    for entry in words:
        combined_entry = [calc_word_value(entry), entry]
        collected_words.append(combined_entry)
        collected_words.sort(key=lambda score: score[0], reverse=True)
    return(collected_words[0][1])
    
