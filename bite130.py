from collections import Counter

import requests


def most_prolific_automaker(year):
    maker_list = [entry['automaker'] for entry in data if entry['year'] == year]
    top_maker = Counter(maker_list)
    most_production = max(top_maker.values())
    return [automaker for automaker in top_maker if top_maker[automaker] == most_production][0]


def get_models(automaker, year):
    car_filter = set()
    for entry in data:
        if entry['automaker'] == automaker and entry['year'] == int(year):
            car_filter.add(entry["model"])
    return car_filter 

