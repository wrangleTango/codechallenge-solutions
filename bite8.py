from collections import deque

def rotate(string, n):
    string = deque(string)
    string.rotate(-1 * n)
    return "".join(string)
    

