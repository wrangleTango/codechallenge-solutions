from datetime import datetime, timedelta


class Promo:
    def __init__(self, name: str, expires: datetime):
        self.name = name
        self.expires = expires
        self._expired = False
        
        @property
        def expired(self):
            """Is it expired?"""
            return self._expired
        
        if self.expires < NOW:
                self._expired = True

