import pytest

def fib(n):
   if n < 0:
        raise ValueError
   elif n in (0, 1):
       return n
   else:
       return(fib(n-1) + fib(n-2))


def test_result_1():
    assert fib(1) == 1

def test_result_2():
    assert fib(2) == 1
    
def test_result_3():
    assert fib(3) == 2
    
def test_result_10():
    assert fib(10) == 55
    
def test_result_20():
    assert fib(20) == 6765

def test_int_out():
    assert type(fib(3)) == int

def test_raise_exception():
    with pytest.raises(ValueError):
        fib(-1)
    

