import string

def load_dictionary():
    """This is a helper function that is part of the problem setup"""
    pass

def is_palindrome(word):
    """Return if word is palindrome, 'madam' would be one.
       Case insensitive, so Madam is valid too.
       It should work for phrases too so strip all but alphanumeric chars.
       So "No 'x' in 'Nixon'" should pass (see tests for more)"""
    lower_word = [letter.lower() for letter in word if letter.lower() in string.ascii_lowercase]
    if lower_word == lower_word[::-1]:
        return word        

def get_longest_palindrome(words=None):
    """Given a list of words return the longest palindrome
       If called without argument use the load_dictionary helper
       to populate the words list"""
    if words == None:
        words = load_dictionary()
    word_and_len = {entry:len(entry) for entry in words if is_palindrome(entry)}
    long_pal = max(word_and_len.values())        
    return [word for word in word_and_len if word_and_len[word] == long_pal][0]