import re


def extract_course_times(course=COURSE):

    time_compile = re.compile(r'\d\d:\d\d')
    return re.findall(time_compile, course)

def get_all_hashtags_and_links(tweet=TWEET):
    link_compile = re.compile('http.*|#\w*')
    return " ".join(re.findall(link_compile, tweet)).split()
    #return [item for item in re.fullmatch(link_compile, tweet)
            #if len(item) > 0]
    #need to try flattening the result of findall in a different way

def match_first_paragraph(html=HTML):
    para_compile = re.compile(r"(<p>)(.*\w.)(</p>)")
    return re.match(para_compile, html).group(2).split("</p>")[0]

def match_blurst_paragraph(html=HTML):
    para_compile = re.compile(r"^(\<p\>)(.*?)(\<\/p\>)")
    return re.match(para_compile, html).groups()[1]
