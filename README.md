# codechallenge solutions

This is a collections of my responses to the codechalleng.es website (pybites).

I can only include the import statements and function definitions, because that's all that's mine. Lots of the challenges also include setup code, but that isn't mine to commit here.

The bulk of these challenges were done from December 2022 to January 2023 where I coded in Python every day. I found the practice and persisting on problems I couldn't grasp initially to be intellectually rewarding.