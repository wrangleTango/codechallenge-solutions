def dedup_and_title_case_names(names):
    names_out = []
    for name in names:
        if name.title() not in names_out:
            names_out.append(name.title())
    return names_out

dedup_names = dedup_and_title_case_names(NAMES)

def sort_by_surname_desc(names):
    sort_list = []
    for name in names:
        name_parts = name.split()
        sort_list.append(name_parts)
    sort_list = sorted(sort_list, key=lambda sort_list: sort_list[1], reverse=True)
    sort_list = [name[0] + " " + name[1] for name in sort_list]
    return sort_list

sort_list = sort_by_surname_desc(dedup_names)

def shortest_first_name(names):
    names = dedup_and_title_case_names(names)
    sort_list = [name.split(" ")[0] for name in names]
    sort_list = sorted(sort_list, key=lambda sort_list: len(sort_list))
    return sort_list[0]    
