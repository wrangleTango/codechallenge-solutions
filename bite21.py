from typing import Dict, List


def get_all_jeeps(cars: CarsType = cars) -> str:
    jeeps = ""
    for model in cars["Jeep"]:
        if jeeps == "":
            jeeps = model
        else:
            jeeps += ", " + model
    return jeeps
        

def get_first_model_each_manufacturer(cars: CarsType = cars) -> List[str]:
    models = [model[0] for model in cars.values()]
    return models
        


def get_all_matching_models(
    cars: CarsType = cars, grep: str = DEFAULT_SEARCH
) -> List[str]:
    matches = []
    for line in cars.values():
        for model in line:
            if grep.lower() in str(model).lower():
                matches.append(model)
    return sorted(matches)

print(get_all_matching_models())

def sort_car_models(cars: CarsType = cars) -> CarsType:
    alpha_cars = {line: sorted(model) for line, model in sorted(cars.items())}
    return alpha_cars
